const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
//const bodyParser = require("body-parser");
require("dotenv/config");

const app = express();
app.use(cors());
app.use(express.json());
app.set("json spaces", 2);
// Or app.use(bodyParser.json)

//Import Routes
const postsRoute = require("./routes/posts");

//Middlewares
app.use("/posts", postsRoute);

//ROUTES
app.get("/", (req, res) => {
  res.send("We are on home");
});

mongoose.connect(process.env.DB_CONNECTION, () =>
  console.log("connected to DB")
);

//Listen to PORT
port = process.env.PORT || 3000;
app.listen(port);
